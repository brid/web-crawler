'use strict';
let request = require('request');
const cheerio = require('cheerio');
const async = require('async');

// Private function
let getLocation = function (path, root, crawledLinks, callback) {
	console.log('---------------------------------');
	console.log('[crawler.js] getLocation() start.');
	console.log('---------------------------------');
	let ret = {internalLinks: [], externalLinks: [], assets: [], location: path};
	try {
		console.log(`[crawler.js] getLocation() crawling location : ${path}`);
		// Request page
		request(path, (err, response, body) => {
			if (err) {
				// Error making the request
				console.log(`[crawler.js] getLocation() request failed : ${err}`);
				callback(err, null);
			} else if (!err && response.statusCode === 200) {
				console.log('[crawler.js] getLocation() request succeeded');
				// Scrape internal pages and assets
				// Use cheerio to parse our raw HTML into a Jquery type object
				const $ = cheerio.load(body);
				// Iterate static content
				let staticContent = [];
				$('img').each(function () {
					const src = $(this).attr('src');
					console.log(`[crawler.js] getLocation() Got static content : ${src}`);
					staticContent.push(src);
				});
				ret.assets = ret.assets.concat(Array.from(new Set(staticContent)));
				// Iterate links
				let internalLinks = [];
				let externalLinks = [];
				$('a').each(function () {
					let href = $(this).attr('href');
					console.log(`[crawler.js] getLocation() Got hyperlink url : ${href}`);
					// Only follow local links
					href = (href.indexOf('http://') === -1 && href.indexOf('https://') === -1) ? root + href : href;
					if (href.indexOf(root) > -1) {
						// Internal link - add to array after removing query-strings or hashes
						let removeQsHash = href.split('?')[0].split('#')[0];
						if (removeQsHash !== '' && href.indexOf('mailto') === -1) {
							console.log('Adding internal link: ' + removeQsHash);
							internalLinks.push(removeQsHash);
						}
					} else {
						// External link
						externalLinks.push(href);
					}
				});
				// Remove duplicates by putting arrays into a set
				const uniqueLinks = new Set(internalLinks);
				// Add our external links
				ret.externalLinks = ret.externalLinks.concat(Array.from(new Set(externalLinks)));
				console.log(`[crawler.js] getLocation() Found ${uniqueLinks.size} unique hyperlinks.`);
				async.eachSeries(Array.from(uniqueLinks), (link, callbackLink) => {
					console.log(`[crawler.js] getLocation() Processing ${link}.`);
					// Ensure we don't do any circular crawling by only following a location once
					if (crawledLinks.has(link)) {
						console.log(`[crawler.js] getLocation() Already indexed [${link}], skipping.`);
						ret.internalLinks.push({internalLinks: [], assets: [], location: link});
						callbackLink(null);
					} else {
						console.log(`[crawler.js] getLocation() Crawling ${link}.`);
						// Add our link to our crawled link set, to prevent it being crawled again
						crawledLinks.add(link);
						getLocation(link, root, crawledLinks, (err, subPages) => {
							if (!err) {
								ret.internalLinks.push(subPages);
								callbackLink(null);
							}
						});
					}
				}, err => {
					console.log(`[crawler.js] getLocation() All hyperlinks iterated.`);
					callback(err, ret);
				});
			} else {
				// Unhandled response, send back empty return object
				callback(null, ret);
			}
		});
	} catch (err) {
		callback(err, null);
	}
};

// Expose our public functions
module.exports = {
	crawlSite: (path, callback) => {
		console.log('[crawler.js] crawlSite() start.');
		// Ensure the path was set
		if (path) {
			const crawledLinks = new Set();
			getLocation(path, path, crawledLinks, (err, result) => {
				if (err) {
					// Error occured
					console.log(`[crawler.js] indexing request failed : ${err}`);
					callback(err, null);
				} else {
					console.log('[crawler.js] indexing request succeeded');
					callback(null, result);
				}
			});
		} else {
			callback('path not set', null);
		}
	}
};
