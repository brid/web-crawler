## Synopsis

As part of a 'hackathon' this simple web crawler was written to index pages and static assets (images) for a given base URL. 

For test purposes the URL is locked on the UI, but can be used to crawl any URL, after adequate input validation has taken place.

## Overview

The project was developed using version 6.6.0 of Node.js and was developed on Fedora Linux 24.

It was not written from a TDD / test first perspective, but unit tests were written in DDT (developer driven testing) style to get code coverage as far up to 100% as possible, within the time frame.

## Installation

Project dependencies are resolved via the following command:

$ npm install

## Running tests

The following commands can be run from within the project directory root to lint and test the application, in addition to giving an overview of test coverage

Performs XO / Eslint linting

$ npm run-script xo

Runs the unit tests

$ npm run-script test

Runs the unit tests and generates a coverage report in the /coverage directory. This can be viewed in a web browser

$ npm run-script coverage

## Running the application

The application can be run with the following command

$ npm start

The web application can be viewed in a web browser at the following location

http://localhost:3000

The application can be ran by clicking the 'crawl' button and allowing indexing to take place behind the scenes. This can take anywhere up to a few minutes to fully crawl the site depending on internet connection speed etc. Please be patient.

After a crawl has taken place, the results of the indexing can be seen as an expandable tree, where nodes can be expanded or collapsed.

## Improvements

- The recursion algorithm was written from scratch with no 'help'. This could be improved by flattening the crawling results to present a more simple means of showing all the crawled pages on a site. Or once a finite list of pages has been crawled, this could be then used by another recursive algorithm to create a site tree that could be used for navigation purposes.
- The crawler is prevented from performing circular indexing by only crawling down a given location once. This results in a skewed object tree, and functions more of a classic crawler than an actual site mapper.
- The crawler is roughly 10x faster using a parallel async request pattern as opposed to running a single http request at any one time. This results in a more consistently populated crawl 'tree' (albeit skewed) at the expense of speed.

## Author

Mark Bridgett [markbridgett@gmail.com](mailto:markbridgett@gmail.com)