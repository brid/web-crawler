const expect = require('chai').expect;
const rewire = require('rewire');

describe("crawler - crawlSite function", () => {

    const crawler = rewire('../lib/crawler');

    it("Should not crawl a site if a path is not set", (done) => {

        crawler.crawlSite(null, (err, json) => {
            check(done,()=>{
                expect(err).to.equal('path not set');
                expect(json).to.be.null;
            })
        });

    });

    it("Should handle an error correctly if a path is set but the initial getLocation call returns an error", (done) => {

        var getLocation = (path, root, crawledLinks, callback) => {
            callback('this is an error');
        };
        crawler.__set__('getLocation', getLocation );
        crawler.crawlSite('http://localhost', (err, obj) => {
            check(done,()=>{
                expect(err).to.equal('this is an error');
                expect(obj).to.be.null;
            })
        });

    });

    it("Should handle a successful response correctly if a path is set", (done) => {

        var getLocation = (path, root, crawledLinks, callback) => {
            callback(null, { message: 'success'});
        };
        crawler.__set__('getLocation', getLocation );
        crawler.crawlSite('http://localhost', (err, obj) => {
            check(done,()=>{
                expect(err).to.be.null;
                expect(obj.message).to.equal('success');
            })
        });

    });

});

describe("crawler - getLocation function", () => {

    const crawler = rewire('../lib/crawler');

    it("Should handle an exception", (done) => {
        var getLocation = crawler.__get__('getLocation');
        var fakeRequest = (url, callback) => {
            throw new Error('exception')
        };
        crawler.__set__('request', fakeRequest );
        getLocation('http://localhost', 'http://localhost', new Set(), (err, obj) => {
            check(done,()=>{
                expect(err.message).to.equal('exception');
                expect(obj).to.be.null;
            })
        });
    });

    it("Should handle an error if an http request fails", (done) => {
        var getLocation = crawler.__get__('getLocation');
        var fakeRequest = (url, callback) => {
            callback('error');
        };
        crawler.__set__('request', fakeRequest );
        getLocation('http://localhost', 'http://localhost', new Set(), (err, obj) => {
            check(done,()=>{
                expect(err).to.equal('error');
                expect(obj).to.be.null;
            })
        });
    });

    it("Should handle a successful request response with an unhandled status code", (done) => {
        var getLocation = crawler.__get__('getLocation');
        var fakeRequest = (url, callback) => {
            callback(null, {}, '');
        };
        crawler.__set__('request', fakeRequest );
        getLocation('http://localhost', 'http://localhost', new Set(), (err, obj) => {
            check(done,()=>{
                expect(err).to.be.null;
                expect(obj).to.deep.equal({internalLinks: [], externalLinks: [], assets: [], location: 'http://localhost'});
            })
        });
    });

    it("Should handle a successful request response with a status code of 200", (done) => {
        var getLocation = crawler.__get__('getLocation');
        var fakeRequest = (url, callback) => {
            callback(null, { statusCode: 200}, '<html></html>');
        };
        crawler.__set__('request', fakeRequest );
        getLocation('http://localhost', 'http://localhost', new Set(), (err, obj) => {
            check(done,()=>{
                expect(err).to.be.null;
                expect(obj).to.deep.equal({internalLinks: [], externalLinks: [], assets: [], location: 'http://localhost'});
            })
        });
    });

    it("Should handle a successful request response with a status code of 200 and static content tags", (done) => {
        var getLocation = crawler.__get__('getLocation');
        var fakeRequest = (url, callback) => {
            callback(null, { statusCode: 200}, '<html><img src="http://localhost/images/test.jpg"></html>');
        };
        crawler.__set__('request', fakeRequest );
        getLocation('http://localhost', 'http://localhost', new Set(), (err, obj) => {
            check(done,()=>{
                expect(err).to.be.null;
                expect(obj).to.deep.equal({internalLinks: [], externalLinks: [], assets: ['http://localhost/images/test.jpg'], location: 'http://localhost'});
            })
        });
    });

    it("Should handle a successful request response with a status code of 200 and external link tags", (done) => {
        var getLocation = crawler.__get__('getLocation');
        var fakeRequest = (url, callback) => {
            callback(null, { statusCode: 200}, '<html><a href="http://www.twitter.com">external link</a></html>');
        };
        crawler.__set__('request', fakeRequest );
        getLocation('http://localhost', 'http://localhost', new Set(), (err, obj) => {
            check(done,()=>{
                expect(err).to.be.null;
                expect(obj).to.deep.equal({internalLinks: [], externalLinks: ['http://www.twitter.com'], assets: [], location: 'http://localhost'});
            })
        });
    });

    it("Should handle a successful request response with a status code of 200 and internal link tags", (done) => {
        var getLocation = crawler.__get__('getLocation');
        var fakeRequest = (url, callback) => {
            callback(null, { statusCode: 200}, '<html><a href="http://localhost">internal link</a></html>');
        };
        crawler.__set__('request', fakeRequest );
        getLocation('http://localhost', 'http://localhost', new Set(), (err, obj) => {
            check(done,()=>{
                expect(err).to.be.null;
                expect(obj).to.deep.equal({internalLinks: [{assets:[], externalLinks: [], internalLinks:[{
                    "assets": [],
                    "internalLinks": [],
                    "location": "http://localhost",
                }], location: 'http://localhost'}], externalLinks: [], assets: [], location: 'http://localhost'});
            })
        });
    });

});

function check( done, f ) {
    try {
        f();
        done();
    } catch(e) {
        done(e);
    }
}