$(document).ready(function() {
    $( '#submit' ).click(function() {
        var baseUrl = $('#baseUrl').val();;
        $.ajax({
            url: '/crawl?baseUrl=' + baseUrl,
            beforeSend: function () {
                $(".modal").show();
            },
            complete: function () {
                $(".modal").hide();
            },
            context: document.body
        }).done(function(json) {
            $('#content').jsonView(JSON.stringify(json), { collapsed: true });
        });
    });
});