const express = require('express');
const crawler = require('../lib/crawler');

const router = express.Router(); // eslint-disable-line babel/new-cap

router.get('/', (req, res) => {
	res.render('index', {title: 'WEB CRAWLER'});
});

router.get('/crawl', (req, res) => {
	let rootUrl = req.query.baseUrl;
	console.log(`Crawling ${rootUrl}`);
	// Crawl the site
	crawler.crawlSite(rootUrl, (err, result) => {
		if (err) {
			res.status(500).send(`Sorry, an error was returned: ${err}`);
		} else {
			// Crawling succeeded
			console.log(`[index.js] router.get('/') Got crawl results: ${JSON.stringify(result)}`);
			res.json(result);
		}
	});
});

module.exports = router;
